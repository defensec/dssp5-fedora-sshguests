Summary: Bash script to configure dssp5-fedora ssh guests
Name: dssp5-fedora-sshguests
Version: 0.1
Release: 1
License: Unlicense
Group: System Environment/Tools
Source: dssp5-fedora-sshguests.tgz
URL: https://github.com/DefenSec/dssp5-fedora-sshguests
Requires: dssp5-fedora glibc-common openssh-server policycoreutils shadow-utils systemd
BuildRequires: argbash make python3-docutils
BuildArch: noarch

%description
Bash script to configure dssp5-fedora ssh guests.

%prep
%autosetup -n dssp5-fedora-sshguests

%build

%install
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%license LICENSE
%{_bindir}/%{name}
%{_mandir}/man1/%{name}.1.gz
%dir %{_datadir}/bash-completion
%dir %{_datadir}/bash-completion/completions
%{_datadir}/bash-completion/completions/%{name}

%changelog
* Tue May 04 2021 Dominick Grift <dominick.grift@defensec.nl> - 0.1-1
- Initial package
