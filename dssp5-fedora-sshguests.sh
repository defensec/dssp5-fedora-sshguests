# SPDX-FileCopyrightText: © 2021 Dominick Grift <dominick.grift@defensec.nl>
# SPDX-License-Identifier: Unlicense

#!/usr/bin/env bash

# Created by argbash-init v2.8.1
# Rearrange the order of options below according to what you would like to see in the help message.
# ARG_OPTIONAL_SINGLE([username],[u],[User name for dssp5-fedora ssh guest])
# ARG_OPTIONAL_SINGLE([password],[p],[Password for dssp5-fedora ssh guest],[abc123])
# ARG_OPTIONAL_BOOLEAN([sftponly],[s],[Transaction applies to sftp-only dssp5-fedora ssh guest])
# ARG_OPTIONAL_BOOLEAN([delete],[d],[Remove dssp5-fedora ssh guest])
# ARGBASH_SET_INDENT([    ])
# ARGBASH_SET_DELIM([ =])
# ARG_OPTION_STACKING([getopt])
# ARG_RESTRICT_VALUES([no-local-options])
# ARG_VERSION_AUTO([1.0.0])
# ARG_VERBOSE([])
# ARG_HELP([Configure dssp5-fedora ssh guests],[I use this to script to configure dssp5-fedora ssh guests])
# ARGBASH_GO()
# needed because of Argbash --> m4_ignore([
### START OF CODE GENERATED BY Argbash v2.10.0 one line above ###
# Argbash is a bash code generator used to get arguments parsing right.
# Argbash is FREE SOFTWARE, see https://argbash.io for more info


die()
{
    local _ret="${2:-1}"
    test "${_PRINT_HELP:-no}" = yes && print_help >&2
    echo "$1" >&2
    exit "${_ret}"
}


evaluate_strictness()
{
    [[ "$2" =~ ^-(-(username|password|sftponly|delete|version|verbose|help)$|[upsdvh]) ]] && die "You have passed '$2' as a value of argument '$1', which makes it look like that you have omitted the actual value, since '$2' is an option accepted by this script. This is considered a fatal error."
}


begins_with_short_option()
{
    local first_option all_short_options='upsdvh'
    first_option="${1:0:1}"
    test "$all_short_options" = "${all_short_options/$first_option/}" && return 1 || return 0
}

# THE DEFAULTS INITIALIZATION - OPTIONALS
_arg_username=
_arg_password="abc123"
_arg_sftponly="off"
_arg_delete="off"
_arg_verbose=0


print_help()
{
    printf '%s\n' "Configure dssp5-fedora ssh guests"
    printf 'Usage: %s [-u|--username <arg>] [-p|--password <arg>] [-s|--(no-)sftponly] [-d|--(no-)delete] [-v|--version] [--verbose] [-h|--help]\n' "$0"
    printf '\t%s\n' "-u, --username: User name for dssp5-fedora ssh guest (no default)"
    printf '\t%s\n' "-p, --password: Password for dssp5-fedora ssh guest (default: 'abc123')"
    printf '\t%s\n' "-s, --sftponly, --no-sftponly: Transaction applies to sftp-only dssp5-fedora ssh guest (off by default)"
    printf '\t%s\n' "-d, --delete, --no-delete: Remove dssp5-fedora ssh guest (off by default)"
    printf '\t%s\n' "-v, --version: Prints version"
    printf '\t%s\n' "--verbose: Set verbose output (can be specified multiple times to increase the effect)"
    printf '\t%s\n' "-h, --help: Prints help"
    printf '\n%s\n' "I use this to script to configure dssp5-fedora ssh guests"
}


parse_commandline()
{
    while test $# -gt 0
    do
        _key="$1"
        case "$_key" in
            -u|--username)
                test $# -lt 2 && die "Missing value for the optional argument '$_key'." 1
                _arg_username="$2"
                shift
                evaluate_strictness "$_key" "$_arg_username"
                ;;
            --username=*)
                _arg_username="${_key##--username=}"
                evaluate_strictness "$_key" "$_arg_username"
                ;;
            -u*)
                _arg_username="${_key##-u}"
                evaluate_strictness "$_key" "$_arg_username"
                ;;
            -p|--password)
                test $# -lt 2 && die "Missing value for the optional argument '$_key'." 1
                _arg_password="$2"
                shift
                evaluate_strictness "$_key" "$_arg_password"
                ;;
            --password=*)
                _arg_password="${_key##--password=}"
                evaluate_strictness "$_key" "$_arg_password"
                ;;
            -p*)
                _arg_password="${_key##-p}"
                evaluate_strictness "$_key" "$_arg_password"
                ;;
            -s|--no-sftponly|--sftponly)
                _arg_sftponly="on"
                test "${1:0:5}" = "--no-" && _arg_sftponly="off"
                ;;
            -s*)
                _arg_sftponly="on"
                _next="${_key##-s}"
                if test -n "$_next" -a "$_next" != "$_key"
                then
                    { begins_with_short_option "$_next" && shift && set -- "-s" "-${_next}" "$@"; } || die "The short option '$_key' can't be decomposed to ${_key:0:2} and -${_key:2}, because ${_key:0:2} doesn't accept value and '-${_key:2:1}' doesn't correspond to a short option."
                fi
                ;;
            -d|--no-delete|--delete)
                _arg_delete="on"
                test "${1:0:5}" = "--no-" && _arg_delete="off"
                ;;
            -d*)
                _arg_delete="on"
                _next="${_key##-d}"
                if test -n "$_next" -a "$_next" != "$_key"
                then
                    { begins_with_short_option "$_next" && shift && set -- "-d" "-${_next}" "$@"; } || die "The short option '$_key' can't be decomposed to ${_key:0:2} and -${_key:2}, because ${_key:0:2} doesn't accept value and '-${_key:2:1}' doesn't correspond to a short option."
                fi
                ;;
            -v|--version)
                printf '%s %s\n\n%s\n' "dssp5-fedora-sshguests.sh" "1.0.0" ''
                exit 0
                ;;
            -v*)
                printf '%s %s\n\n%s\n' "dssp5-fedora-sshguests.sh" "1.0.0" ''
                exit 0
                ;;
            --verbose)
                _arg_verbose=$((_arg_verbose + 1))
                ;;
            -h|--help)
                print_help
                exit 0
                ;;
            -h*)
                print_help
                exit 0
                ;;
            *)
                _PRINT_HELP=yes die "FATAL ERROR: Got an unexpected argument '$1'" 1
                ;;
        esac
        shift
    done
}

parse_commandline "$@"

# OTHER STUFF GENERATED BY Argbash

### END OF CODE GENERATED BY Argbash (sortof) ### ])
# [ <-- needed because of Argbash

test -z $_arg_username && die "user name must be specified"

if [[ $_arg_sftponly == "off" && $_arg_delete == "off" ]]
then

    getent passwd $_arg_username >/dev/null && die "add user failed"

    retval=0
    getent group dssp5-fedora-sshguests >/dev/null || \
        groupadd dssp5-fedora-sshguests
    retval=$?

    test $retval -eq 0 || die "add user failed"

    useradd -G dssp5-fedora-sshguests $_arg_username || die "add user failed"

    retval=0
    echo "$_arg_password" | passwd $_arg_username --stdin
    retval=$?

    test $retval -eq 0 || die "add user failed"

    systemctl mask user@$(id -u $_arg_username).service \
              user-runtime-dir@$(id -u $_arg_username).service || \
        die "add user failed"

    systemctl daemon-reload || die "add user failed"

    if semodule -l | grep ^"dssp5-fedora-sshguests"$ >/dev/null
    then

        semodule -nB || die "add user failed"

        restorecon -RF "$(getent passwd $_arg_username | cut -d: -f6)" || \
            die "add user failed"

        restorecon -RF "/var/spool/mail/${_arg_username}" || \
            die "add user failed"

    else

        test -f dssp5-fedora-sshguests.cil && \
            die "add user failed"

        retval=0
        cat > dssp5-fedora-sshguests.cil <<EOF
(selinuxuser %dssp5-fedora-sshguests sshguests.id (systemlow systemlow))

(userprefix sshguests.id sshguests.role)

(block sshguests

       (blockinherit .user.unpriv.template)

       (call .openssh.server.sftp.role (role))

       (call .passwd.role (role))

       (call .unixupdate.role (role))

       (call .user.openssh.role (role)))
EOF
        retval=$?

        test $retval -eq 0 || die "add user failed"

        semodule -i dssp5-fedora-sshguests.cil || die "add user failed"

        restorecon -RF "$(getent passwd $_arg_username | cut -d: -f6)" || \
            die "add user failed"

        restorecon -RF "/var/spool/mail/${_arg_username}" || \
	    die "add user failed"

        \rm -f -- dssp5-fedora-sshguests.cil || die "add user failed"

        test -f /etc/selinux/dssp5-fedora/contexts/users/sshguests.id || \
            { echo "sys.role:openssh.server.subj:s0 sshguests.role:user.openssh.subj:s0" > \
                   /etc/selinux/dssp5-fedora/contexts/users/sshguests.id || \
                  die "add user failed" ;}

        grep sshguests.id$ /etc/security/sepermit.conf >/dev/null || \
	    { echo "%sshguests.id" >> /etc/security/sepermit.conf || \
                  die "add user failed" ;}


        grep sshguests.role:user.subj$ \
	     /etc/selinux/dssp5-fedora/contexts/default_type >/dev/null || \
	    { echo "sshguests.role:user.subj" >> \
                   /etc/selinux/dssp5-fedora/contexts/default_type || \
                  die "add user failed" ;}

    fi

    test -f /etc/ssh/sshd_config.d/09-dssp5-fedora-sshguests.conf || \
        { retval=0
          cat > /etc/ssh/sshd_config.d/09-dssp5-fedora-sshguests.conf <<EOF
Match Group dssp5-fedora-sshguests
PasswordAuthentication yes
AllowTcpForwarding no
PermitTunnel no
X11Forwarding no
EOF
          retval=$?

          test $retval -eq 0 || die "add user failed" ;}

    systemctl reload sshd || die "add user failed"

elif [[ $_arg_sftponly == "off" && $_arg_delete == "on" ]]
then

    getent passwd $_arg_username >/dev/null || die "delete user failed"

    systemctl unmask user@$(id -u $_arg_username).service \
              user-runtime-dir@$(id -u $_arg_username).service ||
        die "delete user failed"

    systemctl daemon-reload || die "delete user failed"

    \rm -rf -- $(getent passwd $_arg_username | cut -d: -f6) || \
        die "delete user failed"

    userdel -r $_arg_username >/dev/null || die "delete user failed"

    semodule -nBP || die "delete user failed"

elif [[ $_arg_sftponly == "on" && $_arg_delete == "off" ]]
then

    getent passwd $_arg_username >/dev/null && die "user name exists"

    retval=0
    getent group dssp3-sftpguests >/dev/null || groupadd dssp3-sftpguests
    retval=$?

    test $retval -eq 0 || die "add user failed"

    useradd -G dssp5-fedora-sftpguests $_arg_username || die "add user failed"

    chown root.dssp5-fedora-sftpguests \
	  $(getent passwd $_arg_username | cut -d: -f6) || \
        die "add user failed"

    chmod g+x $(getent passwd $_arg_username | cut -d: -f6) || \
        die "add user failed"

    mkdir "$(getent passwd $_arg_username | cut -d: -f6)"/incoming || \
        die "add user failed"

    chown ${_arg_username}.${_arg_username} \
          "$(getent passwd $_arg_username | cut -d: -f6)"/incoming || \
        die "add user failed"

    chmod 0700 \
          "$(getent passwd $_arg_username | cut -d: -f6)"/incoming || \
        die "add user failed"

    retval=0
    echo "$_arg_password" | passwd $_arg_username --stdin
    retval=$?

    test $retval -eq 0 || die "add user failed"

    systemctl mask user@$(id -u $_arg_username).service \
              user-runtime-dir@$(id -u $_arg_username).service || \
        die "add user failed"

    systemctl daemon-reload || die "add user failed"

    if semodule -l | grep ^"dssp3-sftpguests"$ >/dev/null
    then

        semodule -nBP || die "add user failed"

        restorecon -RF "$(getent passwd $_arg_username | cut -d: -f6)" || \
            die "add user failed"

        restorecon -RF "/var/spool/mail/${_arg_username}" || \
            die "add user failed"

    else

        test -f dssp5-fedora-sftpguests.cil && die "add user failed"

        retval=0
        cat > dssp5-fedora-sftpguests.cil <<EOF
(selinuxuser %dssp5-fedora-sftpguests sftpguests.id (systemlow systemlow))

(userprefix sftpguests.id sftpguests.role)

(block sftpguests

       (blockinherit .user.unpriv.template)

       (call .user.openssh.role (role)))
EOF
        retval=$?

        test $retval -eq 0 || die "add user failed"

        semodule -i dssp5-fedora-sftpguests.cil || die "add user failed"

        restorecon -RF "$(getent passwd $_arg_username | cut -d: -f6)" || \
            die "add user failed"

        restorecon -RF "/var/spool/mail/${_arg_username}" || \
            die "add user failed"

        \rm -f -- dssp5-fedora-sftpguests.cil || die "add user failed"

        test -f /etc/selinux/dssp5-fedora/contexts/users/sftpguests.id || \
            { echo "sys.role:openssh.server.subj:s0 sftpguests.role:user.openssh.subj:s0" > \
                   /etc/selinux/dssp5-fedora/contexts/users/sftpguests.id || \
                      die "add user failed" ;}

        grep sftpguests.id$ /etc/security/sepermit.conf >/dev/null || \
            { echo "%sftpguests.id" >> /etc/security/sepermit.conf || \
                  die "add user failed" ;}

        grep sftpguests.role:user.subj$ \
	     /etc/selinux/dssp5-fedora/contexts/default_type >/dev/null || \
	    { echo "sftpguests.role:user.subj" >> \
                   /etc/selinux/dssp5-fedora/contexts/default_type || \
                  die "add user failed" ;}

    fi

    test -f /etc/ssh/sshd_config.d/09-dssp5-fedora-sftpguests.conf || \
        { retval=0
          cat > /etc/ssh/sshd_config.d/09-dssp5-fedora-sftpguests.conf <<EOF
Match Group dssp5-fedora-sftpguests
PasswordAuthentication yes
ChrootDirectory %h
ForceCommand internal-sftp -f AUTHPRIV -l INFO
AllowTcpForwarding no
PermitTunnel no
X11Forwarding no
EOF
          retval=$?

          test $retval -eq 0 || die "add user failed" ;}

    systemctl reload sshd || die "add user failed"

elif [[ $_arg_sftponly == "on" && $_arg_delete == "on" ]]
then

    getent passwd $_arg_username >/dev/null || die "delete user failed"

    systemctl unmask user@$(id -u $_arg_username).service \
              user-runtime-dir@$(id -u $_arg_username).service ||
        die "delete user failed"

    systemctl daemon-reload || die "delete user failed"

    \rm -rf -- $(getent passwd $_arg_username | cut -d: -f6) || \
        die "delete user failed"

    userdel -r $_arg_username >/dev/null || die "delete user failed"

    semodule -nBP || die "delete user failed"

fi

#EOF
# ] <-- needed because of Argbash
